-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 07, 2018 at 02:57 PM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `myspace`
--

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `liked_user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `role` varchar(40) DEFAULT NULL,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `adres` varchar(255) NOT NULL,
  `relation_status` varchar(255) NOT NULL,
  `active` tinyint(1) DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `image`, `email`, `username`, `password`, `salt`, `role`, `firstname`, `lastname`, `adres`, `relation_status`, `active`, `created`, `modified`) VALUES
(1, '', 'test@test.com', '', 'b0b985c8de0bd51993216b2f668a2e59551a780a8f3e80bd3afdee62d523e4b8', '5b9247253e26b', 'user', 'Mikal', 'van Eijk', '', '0', 1, '2018-09-07 09:38:45', NULL),
(2, '', 'dingen@ding.com', '', 'b9f8c1d5ad979525672b554f1fa15be1fb04539e4a948ff2984023d072d3b03a', '5b9249b5d1625', 'user', 'Mikal', 'van Eijk', '', '0', 1, '2018-09-07 09:49:41', NULL),
(3, '', 'mve34@hotmail.nl', '', 'fb7f8098f8d6884f8805ea3fc9e1fd851f5bc6ca7abea43e1876ee0f828c3683', '5b924d5d23d04', 'user', 'Henk', 'van eijk', '', '0', 1, '2018-09-07 10:05:17', '2018-09-07 12:06:58'),
(4, 'penguin.jpg', '2039559@talnet.nl', '', '18af00a105a407954c742782e573a8f6dd9f20e76d70c8f0e9bdb95920c8f977', '5b92687551a13', 'user', 'hans', 'de boer', '34, Gravenland', 'complicated', 1, '2018-09-07 12:00:53', '2018-09-07 14:24:11');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
