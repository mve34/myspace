<?php
/**
 * Created by PhpStorm.
 * User: 2039559
 * Date: 24-9-2018
 * Time: 10:58
 */

class Like extends Model
{

    protected $table = 'likes';

    /**
     * @Type int(11)
     */
    protected $user_id;

    /**
     * @Type int(11)
     */
    protected $liked_user_id;

    public static function newLike()
    {
        // query draaien om te kijken of er een like is voor deze users
        $query = DB::getInstance()->prepare('SELECT * FROM likes WHERE user_id = :user_id AND liked_user_id = :liked_user_id');
        $query->setFetchMode(PDO::FETCH_ASSOC);
        $query->execute([
                'user_id' => App::$user->id,
                'liked_user_id' => $_GET['liked_user_id'],
            ]);
        $query = $query->fetchAll();

        if(count($query) > 0) {
            DB::getInstance()->prepare('DELETE FROM likes WHERE user_id = :user_id AND liked_user_id = :liked_user_id')
                ->execute([
                    'user_id' => App::$user->id,
                    'liked_user_id' => $_GET['liked_user_id'],
                ]);
            dd('if');
        }
        else {
            $like = new Like();
            $like->user_id = App::$user->id;
            $like->liked_user_id = $_GET['liked_user_id'];
            $like->save();
            dd('else');
        }
    }

    protected static function newModel($obj)
    {
        return true;
    }
}