<?php

$guest = true;
$loggedinUser = false;
$admin = false;

if(App::checkAuth(App::ROLE_USER)){
    $guest = false;
    $loggedinUser = true;
}
if(App::checkAuth(App::ROLE_ADMIN)){
    $guest = false;
    $admin = true;
}
$user = User::findByID($_GET['id']);



$like_amount = DB::getInstance()->prepare('SELECT id FROM likes WHERE liked_user_id = :liked_user_id');
$like_amount->setFetchMode(PDO::FETCH_ASSOC);
$like_amount->execute([
    'liked_user_id' => $_GET['id'],
]);
$like_amount = $like_amount->fetchAll();
if($loggedinUser) {
    $like = 'Like';
    $query = DB::getInstance()->prepare('SELECT id FROM likes WHERE user_id = :user_id AND liked_user_id = :liked_user_id');
    $query->setFetchMode(PDO::FETCH_ASSOC);
    $query->execute([
        'user_id' => App::$user->id,
        'liked_user_id' => $_GET['id'],
    ]);
    $query = $query->fetchAll();

    if (count($query) > 0) {
        $like = 'Dislike';
    }
}
?>

<div class="container">

    <div class="blockContainer">

        <div class="block w16">
            <h1>
                <?php echo "welcome to the page of " . $user->getFullname(); ?>
            </h1>
            <h4>
                <div class="left">
                    <p>
                        My username is: <?php echo $user->username; ?><br/>
                        <?php echo "My name is: ". $user->firstname . " " . $user->lastname; ?><br/>
                        <?php if($loggedinUser){echo "My e-mail is: ". $user->email;} ?><br/>
                        <?php if($loggedinUser){echo "My house is located at: " . $user->adres;} ?><br/>
                        <?php if($loggedinUser){echo "And my relation status is: ". $user->relation_status;} ?><br/>
                        <?php if($loggedinUser){echo "This page got ". count($like_amount) ." likes!";} ?><br/>
                    </p>
                </div>
                <div class="right">
                    <img src="<?php echo Http::$webroot.'images/'.$user->getImage(); ?>"/>
                </div>
                <?php if($loggedinUser){ ?>
                    <a <?= App::link('likes&liked_user_id='.$user->id) ?> id="like-button" style="background: green; border-radius: 20%"><?=$like?></a>
                <?php } ?>
            </h4>
        </div>
    </div>
</div>

<script type='text/javascript'>
    //AJAX function
    $('#like-button').click(function(event) {
        event.preventDefault();

        $(this).text(($(this).text() == 'Like') ? 'Dislike' : 'Like');

        $.ajax({
            type: "GET",
            url: $(this).attr('href')
        });
    });

</script>